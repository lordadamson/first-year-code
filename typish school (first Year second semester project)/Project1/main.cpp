#define _CRT_SECURE_NO_DEPRECATE

#include <iostream>
#include <string>
#include <fstream>
#include <conio.h>
#include <locale>
#include <sstream>
#include <windows.h>
#include <iomanip>
#include "Menu.cpp"

#define Up 72
#define Down 80
#define Enter 13

using namespace std;

//*********************************************************************************
							//prototypes.

void past_assignments(string mode, int color);
void delivered_assignments(string mode, int color, string title);
void signin(int color);
void assignment(string mode, int color);
void QandA(string mode, int color);
void schedule(string mode, int color);
void attendance(string mode,int color);
void main_menu(int color);
void change_theme(int color);

//*********************************************************************************
							//misc functions.

void hidecursor()
{
	HANDLE consoleHandle = GetStdHandle(STD_OUTPUT_HANDLE);
	CONSOLE_CURSOR_INFO info;
	info.dwSize = 10;
	info.bVisible = FALSE;
	SetConsoleCursorInfo(consoleHandle, &info);
}
void showcursor()
{
	HANDLE consoleHandle = GetStdHandle(STD_OUTPUT_HANDLE);
	CONSOLE_CURSOR_INFO info;
	info.dwSize = 10;
	info.bVisible = TRUE;
	SetConsoleCursorInfo(consoleHandle, &info);
}
void gotoxy ( short x, short y )
{
	COORD coord = {x, y};
	SetConsoleCursorPosition ( GetStdHandle ( STD_OUTPUT_HANDLE ), coord );
}
string convertInt(int number)
{
	stringstream ss; //create a stringstream
	ss << number; //add number to the stream
	return ss.str(); //return a string with the contents of the stream
}
int convertChar(char myChar)
{
	return myChar - 48;
}
int convertString(string myString)
{
	istringstream buffer(myString);
	int value;
	buffer >> value;
	return value;
}
string hashed(string pass)
{
	locale loc;
	const collate<char>& coll = use_facet<collate<char> >(loc);
	long pass_hashed = coll.hash(pass.data(),pass.data()+pass.length());
	return convertInt(pass_hashed);
}

//**********************************************************************************
							//welcome pack.

void welcome(int color)
{
	hidecursor();
	HANDLE  hConsole;
	hConsole = GetStdHandle (STD_OUTPUT_HANDLE);

	char welcome[27] = {'B', 'e', 'h', 'o', 'l', 'd', ' ', 'T', 'h', 'e', ' ', 'A', 'w', 'e', 's', 'o', 'm', 'e', ' ', 'P', 'r', 'o', 'j', 'e', 'c', 't', '!'};
	char amato[11] = {'A', 'h', 'm', 'a', 'd', ' ', 'R', 'a', 'g', 'a', 'b'};
	char taison[11] = {'A', 'h', 'm', 'e', 'd', ' ', 'E', 's', 's', 'a', 'm'};
	char adam[12] = {'A', 'd', 'h', 'a', 'm', ' ', 'Z', 'a', 'h', 'r', 'a', 'n'};

	for (int i = 0; i <= 25; i++)
	{
		gotoxy(25 + i, 0);
		if (i == 0 || i == 7 || i == 11 || i == 19)
		{
			SetConsoleTextAttribute (hConsole, color);
			cout << welcome[i];
			Sleep(50);
		}
		else
		{
			SetConsoleTextAttribute (hConsole, 15);
			cout << welcome[i];
			Sleep(50);
		}
	}

	for (int i = 0; i <= 10; i++)
	{
		gotoxy(32 + i, 1);
		if (i == 0 || i == 6)
		{
			SetConsoleTextAttribute (hConsole, color);
			cout << amato[i];
			Sleep(50);
		}
		else
		{
			SetConsoleTextAttribute (hConsole, 15);
			cout << amato[i];
			Sleep(50);
		}
	}

	for (int i = 0; i <= 10; i++)
	{
		gotoxy(32 + i, 2);
		if (i == 0 || i == 6)
		{
			SetConsoleTextAttribute (hConsole, color);
			cout << taison[i];
			Sleep(50);
		}
		else
		{
			SetConsoleTextAttribute (hConsole, 15);
			cout << taison[i];
			Sleep(50);
		}
	}

	for (int i = 0; i <= 11; i++)
	{
		gotoxy(32 + i, 3);
		if (i == 0 || i == 6)
		{
			SetConsoleTextAttribute (hConsole, color);
			cout << adam[i];
			Sleep(50);
		}
		else
		{
			SetConsoleTextAttribute (hConsole, 15);
			cout << adam[i];
			Sleep(50);
		}
	}
	Sleep(1500);
}

//**********************************************************************************
							//Home."sweet home".


void home_page(string mode, int color)
{
	Menu menu;
	menu.set_color(color);

	if (mode != "math" &&  mode != "physics" && mode != "electronics" && mode != "application" && mode != "human" && mode != "english" && mode != "structural")
	{		
		switch (menu.Gmenu("Schedule", "Q&A", "Assignments", "Sign out"))
		{
		case 0: schedule(mode, color); system("cls"); break;
		case 1: QandA(mode, color); break;
		case 2: assignment(mode, color); break;
		case 3: main_menu(color); break;
		}
	}
	else
	{
		switch (menu.Gmenu("Schedule", "Q&A", "Assignments", "Attendance", "Sign out"))
		{
		case 0: schedule(mode, color); system("cls"); break;
		case 1: QandA(mode, color); break;
		case 2: assignment(mode, color); break;
		case 3: attendance(mode,color); break;
		case 4: main_menu(color); break;
		}
	}
}

//*******************************************************************************
						//schedule.

string todays_schedule(int &attendance_day,int &attendance_month)  //today is ???
{

	string days[7] = {"sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday"}; 
	int a, month, year, y, day, m, d;
	time_t t = time(0);   // get time now 
	struct tm * now = localtime( & t ); 
	year = now -> tm_year + 1900;
	month = now -> tm_mon + 1; 
	day = now -> tm_mday; 

	attendance_day=day;
	attendance_month=month;

	a = (14 - month)/12;
	y = year - a;
	m = month + 12*a-2; 
	d = (day + y + y/4 - y/100 + y/400 + (31*m/12))%7; 
	return days[d];
}

void show_schedule_week(string mode)
{ 
	system("cls");
	HANDLE  hConsole;
	hConsole = GetStdHandle (STD_OUTPUT_HANDLE);
	int row,col;
	string word,today;
	string pattern[3];
	string schedule [6][4] = {{"", "8-10", "10-12", "12-2"}, 
	{"sunday", "-", "-", "-"},
	{"monday", "-", "-", "-"},
	{"tuesday", "-", "-" ,"-"},
	{"wednesday", "-", "-", "-" },
	{"thursday", "-", "-", "-"},
	};

	ifstream file("schedule.txt");

	if (!file)
	{
		// Print an error and exit
		cerr << "Uh oh, info.txt could not be opened!" << endl;
		exit(1);
	}
	//show student schedule.
	if (mode != "math" &&  mode != "physics" && mode != "electronics" && mode != "application" && mode != "human" && mode != "english" && mode != "structural")
	{
		while(file)
		{
			for(int z = 0; z < 3; z++)
			{
				file >> pattern[z];
			}
			if(pattern[0] == "sunday")
				row = 1;
			else if(pattern[0] == "monday")
				row = 2;
			else if(pattern[0] == "tuesday")
				row = 3;
			else if(pattern[0] == "wednesday")
				row = 4;
			else if(pattern[0] == "thursday")
				row = 5;
			if(pattern[1] == "8-10")
				col = 1;
			else if(pattern[1] == "10-12")
				col = 2;
			else if(pattern[1] == "12-2")
				col = 3;

			schedule[row][col] = pattern[2];
		}
	}
	//show teacer's schedule.
	else
	{
		while (file)
		{
			for(int z = 0; z < 3; z++)
			{
				file >> pattern[z];
			}

			if(pattern[2] == mode)
			{
				if(pattern[0] == "sunday")
					row = 1;
				else if(pattern[0] == "monday")
					row = 2;
				else if(pattern[0] == "tuesday")
					row = 3;
				else if(pattern[0] == "wednesday")
					row = 4;
				else if(pattern[0] == "thursday")
					row = 5;
				if(pattern[1] == "8-10")
					col = 1;
				else if(pattern[1] == "10-12")
					col = 2;
				else if(pattern[1] == "12-2")
					col = 3;

				schedule[row][col] = pattern[2];
			}
		}
	}
	for(int i = 0; i < 6; i++)
	{
		for(int j = 0; j < 4; j++)
		{
			cout << setw(15) << schedule[i][j];
		}
		cout << endl;
	}
}

void show_schedule_day(string mode, string today)
{
	int row,col;
	string pattern[3];
	string schedule[8][4] = {{"", "8-10", "10-12", "12-2"},
	{"sunday", "-", "-", "-"},
	{"monday", "-", "-", "-"},
	{"tuesday", "-", "-" ,"-"},
	{"wednesday", "-", "-", "-" },
	{"thursday", "-", "-", "-"},
	{"friday", "-", "-", "-"},
	{"saturday", "-", "-", "-"},
	};

	ifstream file("schedule.txt");

	if (!file)
	{
		// Print an error and exit
		cerr << "Uh oh, info.txt could not be opened!" << endl;
		exit(1);
	}
	//show student today's schedule.
	if (mode != "math" &&  mode != "physics" && mode != "electronics" && mode != "application" && mode != "human" && mode != "english" && mode != "structural")
	{
		while(file)
		{
			for(int z = 0; z < 3; z++)
			{
				file >> pattern[z];
			}
			if(pattern[0] == "sunday")
				row = 1;
			else if(pattern[0] == "monday")
				row = 2;
			else if(pattern[0] == "tuesday")
				row = 3;
			else if(pattern[0] == "wednesday")
				row = 4;
			else if(pattern[0] == "thursday")
				row = 5;
			if(pattern[1] == "8-10")
				col = 1;
			else if(pattern[1] == "10-12")
				col = 2;
			else if(pattern[1] == "12-2")
				col = 3;

			schedule[row][col] = pattern[2];
		}
	}
	//show student today's schedule.
	else
	{
		while (file)
		{
			for(int z = 0; z < 3; z++)
			{
				file >> pattern[z];
			}

			if(pattern[2] == mode)
			{
				if(pattern[0] == "sunday")
					row = 1;
				else if(pattern[0] == "monday")
					row = 2;
				else if(pattern[0] == "tuesday")
					row = 3;
				else if(pattern[0] == "wednesday")
					row = 4;
				else if(pattern[0] == "thursday")
					row = 5;
				if(pattern[1] == "8-10")
					col = 1;
				else if(pattern[1] == "10-12")
					col = 2;
				else if(pattern[1] == "12-2")
					col = 3;

				schedule[row][col] = pattern[2];
			}
		}
	}


	if(today == "sunday")
		row = 1;
	else if(today == "monday")
		row = 2;
	else if(today == "tuesday")
		row = 3;
	else if(today == "wednesday")
		row = 4;
	else if(today == "thursday")
		row = 5;
	else if (today == "friday")
		row = 6;
	else if (today == "saturday")
		row = 7;

	cout << endl << endl;

	for(int i = 0; i < 8; i++)
	{
		for(int j = 0; j < 4; j++)
		{
			if(i == row)
			{
				cout << setw(15) << schedule[i][j];
			}
			else
				break;
		}
	}

	cout << endl << endl;
}

void delete_class(string mode, int color, string day, string time)
{
	int nString = 0;
	int janja = 2;
	string *array_of_file;
	string garbage;

	fstream file("schedule.txt");

	while (file)
	{
		file >> garbage;
		if (!file)
			break;
		nString++; //number of words in the string.
	}

	file.close();
	file.open("schedule.txt");

	array_of_file = new string[nString+1];

	for (int i = 0; i < nString; i++)
		file >> array_of_file[i];

	file.close();

	for (int i = 2; i < nString; i += 3)
		if (array_of_file[i] == mode)
			if (array_of_file[i-1] == time)
				if (array_of_file[i-2] == day)
				{
					array_of_file[i] = "";
					array_of_file[i-1] = "";
					array_of_file[i-2] = "";
				}

	ofstream ofile("schedule.txt");

	for (int i = 0; i < nString; i++)
	{
		if (array_of_file[i] != "" && i != janja)
			ofile << array_of_file[i] << " ";
		else if(array_of_file[i] != "" && i == janja)
			ofile << array_of_file[i] << endl;
		if (i == janja)
			janja += 3;
	}
	ofile.close();

	delete[] array_of_file;
}

void choose_X_class(string mode, int color)
{
	Menu menu;
	menu.set_color(color);

	int file_pointer = 0;
	string day[8], time[8], garbage;
	string str[8] = {""};
	bool next_page = false;

	while (true)
	{
		fstream file("schedule.txt");

		for(int i = 0; i < 8; i++)
			str[i] = "";
		for(int i = 0; i < 8; i++)
			day[i] = "";
		for(int i = 0; i < 8; i++)
			time[i] = "";

		if (next_page == true)
			file.seekg(file_pointer, ios::beg);

		for (int i = 0; i < 8 && file; i++)
		{
			garbage = "";
			file >> day[i] >> time[i] >> garbage;
			if (garbage == mode)
			{
				str[i] = day[i] + " " + time[i];
			}
			else
				i--;
		}

		file_pointer = file.tellg();

		if(str[7] != "")
		{
			switch (menu.Gmenu(str[0], str[1], str[2], str[3], str[4], str[5], str[6], str[7], "Next Page", "Return"))
			{
				case 0: delete_class(mode, color, day[0], time[0]); schedule(mode, color); next_page = false; break;
				case 1: delete_class(mode, color, day[1], time[1]); schedule(mode, color); next_page = false; break;
				case 2: delete_class(mode, color, day[2], time[2]); schedule(mode, color); next_page = false; break;
				case 3: delete_class(mode, color, day[3], time[3]); schedule(mode, color); next_page = false; break;
				case 4: delete_class(mode, color, day[4], time[4]); schedule(mode, color); next_page = false; break;
				case 5: delete_class(mode, color, day[5], time[5]); schedule(mode, color); next_page = false; break;
				case 6: delete_class(mode, color, day[6], time[6]); schedule(mode, color); next_page = false; break;
				case 7: delete_class(mode, color, day[7], time[7]); schedule(mode, color); next_page = false; break;
				case 8: next_page = true; break;
				case 9: schedule(mode, color); next_page = false; break;
			}
		}
		else
		{
			switch (menu.Gmenu(str[0], str[1], str[2], str[3], str[4], str[5], str[6], "Return"))
			{
				case 0: delete_class(mode, color, day[0], time[0]); schedule(mode, color); next_page = false; break;
				case 1: delete_class(mode, color, day[1], time[1]); schedule(mode, color); next_page = false; break;
				case 2: delete_class(mode, color, day[2], time[2]); schedule(mode, color); next_page = false; break;
				case 3: delete_class(mode, color, day[3], time[3]); schedule(mode, color); next_page = false; break;
				case 4: delete_class(mode, color, day[4], time[4]); schedule(mode, color); next_page = false; break;
				case 5: delete_class(mode, color, day[5], time[5]); schedule(mode, color); next_page = false; break;
				case 6: delete_class(mode, color, day[6], time[6]); schedule(mode, color); next_page = false; break;
				case 7: schedule(mode, color); next_page = false; break;
			}
		}
		file.close();
	}
}

void schedule(string mode, int color) 
{
	HANDLE  hConsole;
	hConsole = GetStdHandle (STD_OUTPUT_HANDLE);
	
	int zero=0;
	int i = 0, k;
	string garbage;
	string day;
	string time;
	string day_check;
	string time_check;
	bool mawgood = false;
	bool lessa = false;
	bool show_schedule = true;
	int j;

	Menu menu;

	menu.set_color(color);

	fstream file("schedule.txt");

	if (!file)
	{
		// Print an error and exit
		cerr << "Uh oh, info.txt could not be opened!" << endl;
		exit(1);
	}

	while (true)
	{
		if (show_schedule == true)
		{
			show_schedule_week(mode);
			show_schedule_day(mode, todays_schedule(zero,zero));
		}
		if (mawgood == true)
		{
			SetConsoleTextAttribute (hConsole, color);
			cout << "chosen date is unavailable" << endl;
			SetConsoleTextAttribute (hConsole, 15);
		}
		mawgood = false;

		menu.clear_screen(false);
		menu.set_xy(25, 14);

		//show student today's schedule.
		if (mode != "math" &&  mode != "physics" && mode != "electronics" && mode != "application" && mode != "human" && mode != "english" && mode != "structural")
		{
			switch (menu.Gmenu("show schedule for another day", "home page"))
			{
			case 0:
				{
					menu.clear_screen(true);
					menu.set_xy(33, 5);
					switch (menu.Gmenu("sunday", "monday", "tuesday", "wednesday", "thursday"))
					{
					case 0: show_schedule = false; show_schedule_week(mode); show_schedule_day(mode, "sunday"); break;
					case 1: show_schedule = false; show_schedule_week(mode); show_schedule_day(mode, "monday"); break;
					case 2: show_schedule = false; show_schedule_week(mode); show_schedule_day(mode, "tuesday"); break;
					case 3: show_schedule = false; show_schedule_week(mode); show_schedule_day(mode, "wednesday"); break;
					case 4: show_schedule = false; show_schedule_week(mode); show_schedule_day(mode, "thursday"); break;
					}
					break;
				}
			case 1: home_page(mode, color); break;
			}
		}
		else
		{
			switch (menu.Gmenu("add a class", "delete a class" , "show schedule for another day", "home page"))
			{
			case 0:
				{
					menu.clear_screen(true);
					menu.set_xy(33, 5);
					switch (menu.Gmenu("sunday", "monday", "tuesday", "wednesday", "thursday"))
					{
						case 0: day = "sunday"; break;
						case 1: day = "monday"; break;
						case 2: day = "tuesday"; break;
						case 3: day = "wednesday"; break;
						case 4: day = "thursday"; break;
					}

					switch (menu.Gmenu("8-10", "10-12", "12-2"))
					{
						case 0: time = "8-10"; break;
						case 1: time = "10-12"; break;
						case 2: time = "12-2"; break;
					}

					file.close();
					file.open("schedule.txt");

					while (file >> day_check) //check availaibilty of date of lectures.
					{

						if(day_check == day)
						{
							file.seekg(1, ios::cur);
							file >> time_check;
							if (time_check == time)
							{
								mawgood = true;
								break;
							}
						}
					}

					file.close();

					if (mawgood == false) //save lectures dates in (schedule file).
					{
						file.open("schedule.txt", ios::app);
						file << day << " " << time << " " << mode << endl;
						file.close();
					}
					break;
				}
			case 1: choose_X_class(mode, color); break;
			case 2:
			{
				menu.clear_screen(true);
				menu.set_xy(33, 5);
				switch (menu.Gmenu("sunday", "monday", "tuesday", "wednesday", "thursday"))
				{
					case 0: show_schedule = false; show_schedule_week(mode); show_schedule_day(mode, "sunday"); break;
					case 1: show_schedule = false; show_schedule_week(mode); show_schedule_day(mode, "monday"); break;
					case 2: show_schedule = false; show_schedule_week(mode); show_schedule_day(mode, "tuesday"); break;
					case 3: show_schedule = false; show_schedule_week(mode); show_schedule_day(mode, "wednesday"); break;
					case 4: show_schedule = false; show_schedule_week(mode); show_schedule_day(mode, "thursday"); break;
				}
				break;
			}
			case 3: home_page(mode, color); break;
			}
		}
	}
}

//*******************************************************************************
						//Questions&Answers.

void answers(string question, int color, string mode)
{
	string answer_check;
	string user;
	string pass_check;
	string type_check;
	string answer;
	string answer_digit = "";
	string question_digit = "";

	system("cls");

	fstream file("user.txt");

	if (!file)
	{
		// Print an error and exit
		cerr << "Uh oh, info.txt could not be opened!" << endl;
		exit(1);
	}

	if (mode == "math" ||  mode == "physics" || mode == "electronics" || mode == "application" || mode == "human" || mode == "english" || mode == "structural")
	{	
		while (file)
		{
			file >> user >> pass_check >> type_check;
			if (type_check == mode)
				break;
		}
		file.close();
	}

	cout << "Question: " << question << endl << endl;

	file.close();

	file.open("answers.txt");

	if (!file)
	{
		// Print an error and exit
		cerr << "Uh oh, info.txt could not be opened!" << endl;
		exit(1);
	}

	for (int i = 0; i < question.size(); i++)
	{
		if (question[i] >= 48 && question[i] <= 57)
			question_digit += question[i];
		else break;
	}

	while (file)
	{
		getline(file, answer_check);
		for (int i = 0; i < answer_check.size(); i++)
		{
			if (answer_check[i] >= 48 && answer_check[i] <= 57)
				answer_digit += answer_check[i];
			else break;
		}

		if (question_digit == answer_digit)
		{
			answer_check.erase(0, question_digit.size()+1);
			cout << answer_check << endl;
		}
		answer_digit = "";
	}
	cout << endl;

	file.close();

	cout << "Your answer: ";
	cin.clear();
	cin.sync();
	getline(cin, answer);

	if (answer != "")
	{
		file.open("answers.txt", ios::app);

		if (mode == "math" ||  mode == "physics" || mode == "electronics" || mode == "application" || mode == "human" || mode == "english" || mode == "structural")
			file << question_digit << " Mr/s. " << user << "'s answer: " << answer << endl;
		else
			file << question_digit << " " << mode << "'s answer: " << answer << endl;
	}
	QandA(mode, color);
}

void QandA(string mode, int color)
{
	string questions_menu[9];
	int nQuestion=0;
	bool empty = false;
	string qstr,question_check,question_number,question_username, question_digit = "";

	HANDLE  hConsole;
	hConsole = GetStdHandle (STD_OUTPUT_HANDLE);

	Menu menu;
	menu.set_color(color);
	menu.set_xy(25, 5);

	fstream file("questions.txt");

	if (!file)
	{
		// Print an error and exit
		cerr << "Uh oh, info.txt could not be opened!" << endl;
		exit(1);
	}

	if (mode != "math" &&  mode != "physics" && mode != "electronics" && mode != "application" && mode != "human" && mode != "english" && mode != "structural")
	{
		switch(menu.Gmenu("Post a new question to QZONE", "Put an answer to an existing question", "Home page"))
		{
		case 0:
			{
				system("cls");
				cin.clear();
				cin.sync();
				cout << "Post your question here please: "; 
				SetConsoleTextAttribute (hConsole, color);
				getline(cin, qstr);
				SetConsoleTextAttribute (hConsole, 15);
				if (qstr != "")
				{
					while(file)
					{
						getline(file, question_check);
						if(question_check.size() == 0)
						{
							empty = true;
							break;
						}
						for (int i = 0; i < question_check.size(); i++)
						{
							if (question_check[i] >= 48 && question_check[i] <= 57)
								question_digit += question_check[i];
							else break;
						}
						if(convertString(question_digit) > nQuestion)
							nQuestion = convertString(question_digit);
						question_digit = "";
					}
					if (empty == true)
					{
						file.close();
						file.open("questions.txt");
						file << "1 " <<mode << ": " << qstr;
						file.close();
					}
					else
					{
						nQuestion++;
						file.close();
						file.open("questions.txt" , ios::app);
						file << endl << nQuestion << " " << mode << ": " << qstr;
						file.close();
					}
				}
				QandA(mode,color);
			}
		case 1:
			{
				while (true)
				{
					for (int i = 0; i < 9; i++)
						questions_menu[i] = "";

					for(int i = 0; i < 9; i++)
					{
						getline(file , questions_menu[i]);
					}
					if (questions_menu[8] != "")
					{
						switch(menu.Gmenu(questions_menu[0],questions_menu[1],questions_menu[2],questions_menu[3],questions_menu[4],questions_menu[5],questions_menu[6],questions_menu[7],"Next page", "Return to QZONE"))
						{
						case 0: answers(questions_menu[0], color, mode); break;
						case 1: answers(questions_menu[1], color, mode); break;
						case 2: answers(questions_menu[2], color, mode); break;
						case 3: answers(questions_menu[3], color, mode); break;
						case 4: answers(questions_menu[4], color, mode); break;
						case 5: answers(questions_menu[5], color, mode); break;
						case 6: answers(questions_menu[6], color, mode); break;
						case 7: answers(questions_menu[7], color, mode); break;
						case 8: break;
						case 9: QandA(mode, color); break;
						}
					}
					else
					{
						switch(menu.Gmenu(questions_menu[0],questions_menu[1],questions_menu[2],questions_menu[3],questions_menu[4],questions_menu[5],questions_menu[6],questions_menu[7],questions_menu[8], "Return to QZONE"))
						{
						case 0: answers(questions_menu[0], color, mode); break;
						case 1: answers(questions_menu[1], color, mode); break;
						case 2: answers(questions_menu[2], color, mode); break;
						case 3: answers(questions_menu[3], color, mode); break;
						case 4: answers(questions_menu[4], color, mode); break;
						case 5: answers(questions_menu[5], color, mode); break;
						case 6: answers(questions_menu[6], color, mode); break;
						case 7: answers(questions_menu[7], color, mode); break;
						case 8: answers(questions_menu[8], color, mode); break;
						case 9: QandA(mode, color); break;
						}
					}
				}
			}
		case 2: home_page(mode, color); break;
		}	
	}
	else
	{
		while (true)
		{
			for (int i = 0; i < 9; i++)
				questions_menu[i] = "";

			for(int i = 0; i < 9; i++)
			{
				getline(file , questions_menu[i]);
			}
			if (questions_menu[8] != "")
			{
				switch(menu.Gmenu(questions_menu[0],questions_menu[1],questions_menu[2],questions_menu[3],questions_menu[4],questions_menu[5],questions_menu[6],questions_menu[7],"Next page", "Return to QZONE"))
				{
				case 0: answers(questions_menu[0], color, mode); break;
				case 1: answers(questions_menu[1], color, mode); break;
				case 2: answers(questions_menu[2], color, mode); break;
				case 3: answers(questions_menu[3], color, mode); break;
				case 4: answers(questions_menu[4], color, mode); break;
				case 5: answers(questions_menu[5], color, mode); break;
				case 6: answers(questions_menu[6], color, mode); break;
				case 7: answers(questions_menu[7], color, mode); break;
				case 8: break;
				case 9: QandA(mode, color); break;
				}
			}
			else
			{
				switch(menu.Gmenu(questions_menu[0],questions_menu[1],questions_menu[2],questions_menu[3],questions_menu[4],questions_menu[5],questions_menu[6],questions_menu[7],questions_menu[8], "Return to home page"))
				{
				case 0: answers(questions_menu[0], color, mode); break;
				case 1: answers(questions_menu[1], color, mode); break;
				case 2: answers(questions_menu[2], color, mode); break;
				case 3: answers(questions_menu[3], color, mode); break;
				case 4: answers(questions_menu[4], color, mode); break;
				case 5: answers(questions_menu[5], color, mode); break;
				case 6: answers(questions_menu[6], color, mode); break;
				case 7: answers(questions_menu[7], color, mode); break;
				case 8: answers(questions_menu[8], color, mode); break;
				case 9: home_page(mode, color); break;
				}
			}
		}
	}
}

//*******************************************************************************
							//assignments.

void add_assignment(string mode,int color)
{
	HANDLE  hConsole;
	hConsole = GetStdHandle (STD_OUTPUT_HANDLE);

	fstream file("assignments.txt", ios::app);

	string title,content;
	system("cls");

	cin.clear();
	cin.sync();

	cout << "Title: ";
	SetConsoleTextAttribute (hConsole, color);
	getline(cin, title);
	SetConsoleTextAttribute (hConsole, 15);
	if (title == "")
		assignment(mode, color);
	cout << endl << "Content: ";
	SetConsoleTextAttribute (hConsole, color);
	getline(cin, content);
	SetConsoleTextAttribute (hConsole, 15);

	file << mode << endl;
	file << title << endl;
	file << content << endl;

	file.close();

	assignment(mode, color);
}

struct assignments1
{
	string teacher;
	string title[8];
	string content[8];
	string students[8];
	string path[8];
};

void writing_rates(string mode, string title, string student, string grade)
{
	fstream file("assignment ratings.txt", ios::app);

	string *file_arr;
	int nWords = 0;
	int j = 0;
	string garbage;

	file << mode << endl << student << endl << title << endl << grade << endl;
	file.close();
	file.open("delivered assignments.txt");
	if(!file)
	{
		cerr << "yoyo" << endl;
		exit(1);
	}
	while(file)
	{
		file >> garbage;
		nWords++;
	}
	file_arr = new string[nWords+1];

	file.close();
	file.open("delivered assignments.txt");

	j=0;
	
	while(file)
	{
		getline(file, garbage);
		if(garbage == "")
			break;
		file_arr[j] = garbage;
		j++;
	}

	for(int i = 0; i < nWords; i++)
	{
		if (file_arr[i] == title)
			if(file_arr[i+1] == mode)
				if(file_arr[i+2] == student)
				{
					file_arr[i] = "";
					file_arr[i+1] = "";
					file_arr[i+2] = "";
					file_arr[i+3] = "";
					break;
				}
	}

	file.close();
	file.open("delivered assignments.txt", ios::out);

	for(int i = 0; i < nWords; i++)
	{
		garbage = file_arr[i];
		if (garbage != "")
			file << garbage << endl;
	}
}

void rating_assignments(string mode, int color, string title, string student, string path)
{
	Menu menu;
	menu.set_color(color);
	menu.set_xy(25, 14);
	menu.clear_screen(false);

	string garbage;
	string *file_arr;
	int nLines = 0;
	int nWords = 0;
	int j = 0;

	ifstream file(path);

	if(!file)
	{
		cerr << "Couldn't open desired file" << endl;
		exit(1);
	}
	while(file)
	{
		system("cls");
		cout << student << "'s submission for \"" << title << "\" assignment:" << endl << endl;

		while (file)
		{
			getline(file, garbage);
			if(!file)
				break;
			nLines++;
			cout << garbage << endl;
		}
			switch(menu.Gmenu("Rate this submission", "Return"))
			{
				case 0:
					{
						system("cls");
						cout << student << "'s submission for \"" << title << "\" assignment:" << endl;
						file.close();
						file.open(path);
						while (file)
						{
							getline(file, garbage);
							if(!file)
								break;
							cout << garbage << endl;
						}
						switch(menu.Gmenu("Excellent", "Very good", "Good", "Weak"))
						{
							case 0: writing_rates(mode, title, student, "Excellent"); break;
							case 1: writing_rates(mode, title, student, "Very good"); break;
							case 2: writing_rates(mode, title, student, "good"); break;
							case 3: writing_rates(mode, title, student, "Weak"); break;
						}
						break;
					}
				case 1: delivered_assignments(mode, color, title); break;
			}
		}
	}

void delivered_assignments(string mode, int color, string title)
{
	Menu menu;
	menu.set_color(color);
	menu.clear_screen(false);

	assignments1 d_assignment;///struct ahuh.. :D

	string teacher;
	string title_check;
	string garbage;
	string garbage2;
	int file_pointer = 0;
	bool next_page = false;

	//another menu of the students who delivered.
	while(true)
	{
		fstream file("delivered assignments.txt");

		system("cls");

		cout << "Students who delivered this assignment";

		if(next_page == true)
			file.seekg(file_pointer, ios::beg);
		for (int i = 0; i < 8; i++)
		{
			d_assignment.students[i] = "";
			d_assignment.path[i] = "";
		}


		for(int i = 0; i < 8 || !EOF; i++)
		{
			getline(file, title_check);
			getline(file, teacher);
			getline(file, garbage);
			getline(file, garbage2);
		

			if(!file)
				break;

			if(title_check == title)
			{
				if(teacher == mode)
				{
					d_assignment.students[i] = garbage;
					d_assignment.path[i] = garbage2;
				}
				else
					i--;
			}
			else
				i--;
		}

			file_pointer = file.tellg();
			
			if(d_assignment.students[7] != "")

			{
				switch(menu.Gmenu(d_assignment.students[0], d_assignment.students[1], d_assignment.students[2], d_assignment.students[3], d_assignment.students[4], d_assignment.students[5], d_assignment.students[6], d_assignment.students[7], "Next Page", "Return to past assignments"))
				{
					case 0: rating_assignments(mode, color, title, d_assignment.students[0], d_assignment.path[0]); next_page = false; break;
					case 1: rating_assignments(mode, color, title, d_assignment.students[1], d_assignment.path[1]); next_page = false; break;
					case 2: rating_assignments(mode, color, title, d_assignment.students[2], d_assignment.path[2]); next_page = false; break;
					case 3: rating_assignments(mode, color, title, d_assignment.students[3], d_assignment.path[3]); next_page = false; break;
					case 4: rating_assignments(mode, color, title, d_assignment.students[4], d_assignment.path[4]); next_page = false; break;
					case 5: rating_assignments(mode, color, title, d_assignment.students[5], d_assignment.path[5]); next_page = false; break;
					case 6: rating_assignments(mode, color, title, d_assignment.students[6], d_assignment.path[6]); next_page = false; break;
					case 7: rating_assignments(mode, color, title, d_assignment.students[7], d_assignment.path[7]); next_page = false; break;
					case 8: next_page = true;break;
					case 9: past_assignments(mode, color);break;
				}
			}
			else
			{
				switch(menu.Gmenu(d_assignment.students[0], d_assignment.students[1], d_assignment.students[2], d_assignment.students[3], d_assignment.students[4], d_assignment.students[5], d_assignment.students[6], d_assignment.students[7], "Return to past assignments"))
				{
					case 0: rating_assignments(mode, color, title, d_assignment.students[0], d_assignment.path[0]); next_page = false; break;
					case 1: rating_assignments(mode, color, title, d_assignment.students[1], d_assignment.path[1]); next_page = false; break;
					case 2: rating_assignments(mode, color, title, d_assignment.students[2], d_assignment.path[2]); next_page = false; break;
					case 3: rating_assignments(mode, color, title, d_assignment.students[3], d_assignment.path[3]); next_page = false; break;
					case 4: rating_assignments(mode, color, title, d_assignment.students[4], d_assignment.path[4]); next_page = false; break;
					case 5: rating_assignments(mode, color, title, d_assignment.students[5], d_assignment.path[5]); next_page = false; break;
					case 6: rating_assignments(mode, color, title, d_assignment.students[6], d_assignment.path[6]); next_page = false; break;
					case 7: rating_assignments(mode, color, title, d_assignment.students[7], d_assignment.path[7]); next_page = false; break;
					case 8: past_assignments(mode, color); break;
				}
			}
		file.close();
	}
}

void past_assignments(string mode, int color)
{
	Menu menu;
	menu.set_color(color);

	assignments1 st_assignment;

	string garbage;
	string garbage2;
	int file_pointer = 0;
	bool next_page = false;

	while(true)
	{
		fstream file("assignments.txt");

		if (!file)
		{
			// Print an error and exit
			cerr << "Uh oh, info.txt could not be opened!" << endl;
			exit(1);
		}

		for(int i = 0; i < 8; i++)
			st_assignment.title[i] = "";

		if (next_page == true)
			file.seekg(file_pointer, ios::beg);

		for (int i = 0; i < 8 || !EOF ;i++)
		{
			getline(file, st_assignment.teacher);
			getline(file, garbage);
			getline(file, garbage2);

			if(!file)
				break;

			if (st_assignment.teacher == mode)
			{
				st_assignment.title[i] = garbage;
				st_assignment.content[i] = garbage2; 
			}
			else
				i--;
		}

		file_pointer = file.tellg();

		if(st_assignment.title[7] != "")
		{
			switch(menu.Gmenu(st_assignment.title[0],st_assignment.title[1],st_assignment.title[2],st_assignment.title[3],st_assignment.title[4],st_assignment.title[5],st_assignment.title[6],st_assignment.title[7],"Next page","Return"))
			{
			case 0: delivered_assignments(mode, color, st_assignment.title[0]); next_page = false; break;
			case 1: delivered_assignments(mode, color, st_assignment.title[1]); next_page = false; break;
			case 2: delivered_assignments(mode, color, st_assignment.title[2]); next_page = false; break;
			case 3: delivered_assignments(mode, color, st_assignment.title[3]); next_page = false; break;
			case 4: delivered_assignments(mode, color, st_assignment.title[4]); next_page = false; break;
			case 5: delivered_assignments(mode, color, st_assignment.title[5]); next_page = false; break;
			case 6: delivered_assignments(mode, color, st_assignment.title[6]); next_page = false; break;
			case 7: delivered_assignments(mode, color, st_assignment.title[6]); next_page = false; break;
			case 8: next_page = true; break;
			case 9: assignment(mode, color); break;
			}
		}
		else
		{
			switch(menu.Gmenu(st_assignment.title[0],st_assignment.title[1],st_assignment.title[2],st_assignment.title[3],st_assignment.title[4],st_assignment.title[5],st_assignment.title[6],st_assignment.title[7],"Return"))
			{
			case 0: delivered_assignments(mode, color, st_assignment.title[0]); next_page = false; break;
			case 1: delivered_assignments(mode, color, st_assignment.title[1]); next_page = false; break;
			case 2: delivered_assignments(mode, color, st_assignment.title[2]); next_page = false; break;
			case 3: delivered_assignments(mode, color, st_assignment.title[3]); next_page = false; break;
			case 4: delivered_assignments(mode, color, st_assignment.title[4]); next_page = false; break;
			case 5: delivered_assignments(mode, color, st_assignment.title[5]); next_page = false; break;
			case 6: delivered_assignments(mode, color, st_assignment.title[6]); next_page = false; break;
			case 7: delivered_assignments(mode, color, st_assignment.title[6]); next_page = false; break;
			case 8: assignment(mode, color); break;
			}
		}
		file.close();
	}
}

void display_assignments(string mode, int color, string title, string content, string subject)
{
	Menu menu;
	menu.set_color(color);
	menu.set_xy(25, 14);
	menu.clear_screen(false);

	fstream file;

	system("cls");

	string path;

	HANDLE  hConsole;
	hConsole = GetStdHandle (STD_OUTPUT_HANDLE);

	cout << title  << ":"<< endl;
	cout << "=================" << endl << endl;
	cout << content << endl << endl;

	switch (menu.Gmenu("Enter the path to your submission", "Return"))
	{
		case 0:
			{
				system("cls");
				SetConsoleTextAttribute (hConsole, color);
				cout << "Please insert the path to your submission" <<endl;
				SetConsoleTextAttribute (hConsole, 15);
				
				while(true)
				{
					cin.clear();
					cin.sync();
					getline(cin, path);

					file.open(path);
					if(!file)
					{
						cerr << "Please enter valid path" << endl;
					}
					else
						break;
				}
				file.close();
				file.open("delivered assignments.txt", ios::out);
				file << title << endl << subject << endl << mode << endl << path << endl;
				file.close();
				assignment(mode, color);
			}
		case 1: assignment(mode, color); break;
	}

	
}

void display_rating(string mode, int color, string title, string rating, string subject)
{}

void submit_assignment(string mode, int color, string subject)
{
	Menu menu;
	menu.set_color(color);

	bool next_page = false;
	string title[8];
	string title_check;
	string content[8];
	string content_check;
	string subject_check;
	string student_check;
	string rating_check;
	string ratings[8];
	int file_pointer = 0;

	switch(menu.Gmenu("See new assignments", "See ratings to your assignments", "Return to home page"))
	{
		case 0:
			{
				while (true)
				{
					fstream file("assignments.txt");

					for(int i = 0; i < 8; i++)
						title[i] = "";
					for(int i = 0; i < 8; i++)
						content[i] = "";

					if (next_page == true)
						file.seekg(file_pointer, ios::beg);

					for (int i = 0; i < 8 || !EOF ;i++)
					{
						getline(file, subject_check);
						getline(file, title_check);
						getline(file, content_check);

						if(!file)
							break;

						if (subject_check == subject)
						{
							title[i] = title_check;
							content[i] = content_check;
						}
						else
							i--;
					}

					file_pointer = file.tellg();
					
					menu.clear_screen(true);

					if(title[7] != "")
					{
						switch(menu.Gmenu(title[0],title[1],title[2],title[3],title[4],title[5],title[6],title[7],"Next page","Return"))
						{
						case 0: display_assignments(mode, color, title[0], content[0], subject); next_page = false; break;
						case 1: display_assignments(mode, color, title[1], content[1], subject); next_page = false; break;
						case 2: display_assignments(mode, color, title[2], content[2], subject); next_page = false; break;
						case 3: display_assignments(mode, color, title[3], content[3], subject); next_page = false; break;
						case 4: display_assignments(mode, color, title[4], content[4], subject); next_page = false; break;
						case 5: display_assignments(mode, color, title[5], content[5], subject); next_page = false; break;
						case 6: display_assignments(mode, color, title[6], content[6], subject); next_page = false; break;
						case 7: display_assignments(mode, color, title[7], content[7], subject); next_page = false; break;
						case 8: next_page = true; break;
						case 9: assignment(mode, color); break;
						}
					}
					else
					{
						switch(menu.Gmenu(title[0],title[1],title[2],title[3],title[4],title[5],title[6],title[7],"Return"))
						{
						case 0: display_assignments(mode, color, title[0], content[0], subject); next_page = false; break;
						case 1: display_assignments(mode, color, title[1], content[1], subject); next_page = false; break;
						case 2: display_assignments(mode, color, title[2], content[2], subject); next_page = false; break;
						case 3: display_assignments(mode, color, title[3], content[3], subject); next_page = false; break;
						case 4: display_assignments(mode, color, title[4], content[4], subject); next_page = false; break;
						case 5: display_assignments(mode, color, title[5], content[5], subject); next_page = false; break;
						case 6: display_assignments(mode, color, title[6], content[6], subject); next_page = false; break;
						case 7: display_assignments(mode, color, title[7], content[7], subject); next_page = false; break;
						case 8: assignment(mode, color); break;
						}
					}

				file.close();
				}
			}
		case 1:
			{
				while (true)
				{
					fstream file("assignment ratings.txt");

					for(int i = 0; i < 8; i++)
						title[i] = "";
					for(int i = 0; i < 8; i++)
						ratings[i] = "";


					if (next_page == true)
						file.seekg(file_pointer, ios::beg);

					for (int i = 0; i < 8 || !EOF ;i++)
					{
						getline(file, subject_check);
						getline(file, student_check);
						getline(file, title_check);
						getline(file, rating_check);

						if(!file)
							break;

						if (subject_check == subject)
						{
							if (student_check == mode)
							{
								title[i] = title_check;
								ratings[i] = rating_check;
							}
							else
								i--;
						}
						else
							i--;
					}

					file_pointer = file.tellg();

					system("cls");

					for(int i = 0; i < 8; i++)
						cout << title[i] + " " + ratings[i] << endl;

					menu.clear_screen(false);
					menu.set_xy(33, 5);

					if (title[7] != "")
					{
						switch(menu.Gmenu("Next page","Return"))
						{
							case 0: next_page = true; break;
							case 1: assignment(mode, color); break;
						}
					}
					else
					{
						switch(menu.Gmenu("Return", ""))
						{
							case 0: assignment(mode, color); break;
						}
					}
				file.close();
				}
			}
		case 2: home_page(mode, color); break;
	}

}

void assignment(string mode, int color)
{
	Menu menu;
	menu.set_color(color);

	HANDLE  hConsole;
	hConsole = GetStdHandle (STD_OUTPUT_HANDLE);

	if (mode != "math" &&  mode != "physics" && mode != "electronics" && mode != "application" && mode != "human" && mode != "english" && mode != "structural")
	{
		switch(menu.Gmenu("Math", "Physics", "Electronics", "Application software", "Human writes", "English", "Structural Programming", "Home page"))
		{
			case 0: submit_assignment(mode, color, "math"); break;
			case 1: submit_assignment(mode, color, "physics"); break;
			case 2: submit_assignment(mode, color, "electronics"); break;
			case 3: submit_assignment(mode, color, "application"); break;
			case 4: submit_assignment(mode, color, "human"); break;
			case 5: submit_assignment(mode, color, "english"); break;
			case 6: submit_assignment(mode, color, "structural"); break;
			case 7: home_page(mode, color); break;
		}
	}
	else
	{
		switch(menu.Gmenu("Add a new assignment", "See past assignments", "Home Page"))
		{
			case 0:	add_assignment(mode, color); break;
			case 1: past_assignments(mode, color); break;
			case 2: home_page(mode, color); break;
		}
	}
}

//*********************************************************************************
							//attendance.

struct user1
{
	string students[8];
	string student;
	string type;
	int day;
	int month;
}user;

struct attendancecase
{
	string teacher; 
	string student;
	string day;
	string month;
	string acase;
}attendance1;

void attendance_write(string mode,int color,user1 user,attendancecase attendance1)
{
	Menu menu;
	menu.set_color(color);
	menu.clear_screen(false);

	string garbage , acase;
	string *file_arr;
	int nWords = 0, index , i ;
	bool found=false;

	HANDLE  hConsole;
	hConsole = GetStdHandle (STD_OUTPUT_HANDLE);

	system("cls");

	for(i=0 ; i < user.students[i].size() ; i++)
		if(user.students[i][0] == '.')
		{
			index=i;
			user.students[i].erase(0,1);
			for (int j = 0; user.students[i][j]; j++)
				if (user.students[i][j] == ' ')
				{
					user.students[i].erase(j , user.students[i].size()-j);
					break;
				}
		}

		cout << "The student: ";
		SetConsoleTextAttribute (hConsole, color);
		cout << user.students[index] ;
		SetConsoleTextAttribute (hConsole, 15);
		cout << " Date: "<< user.day << "/" << user.month << "/2012 " <<endl; 

		switch(menu.Gmenu("Present", "Absent", "Back"))
		{
		case 0: acase = "present"; break;
		case 1: acase = "absent"; break;
		case 2: attendance(mode, color); break;
		}

		fstream file("attendance.txt");
		if(!file)
		{
			cerr << "el file mfat7sh" << endl; 
			exit(1);
		}
		//get number of words in our file.
		while(file >> garbage)
		{
			nWords++;
		}

		file_arr = new string[nWords+1];
		file.close();
		file.open("attendance.txt");

		i=0;
		while(file)
		{
			file >> garbage;
			file_arr[i] = garbage;
			i++;
		}

		for( i = 0; i <= nWords; i+=5)
		{
			if(file_arr[i] == mode)
			{
				if(file_arr[i+1] == user.students[index])
					if(convertString(file_arr[i+2]) == user.day)
						if(convertString(file_arr[i+3]) == user.month)
						{
							file_arr[i+4]=acase; 
							found=true;
						}
			}
		}
		file.close();
		if (found == false)
		{
			file.open("attendance.txt" , ios::app);
			file << mode << " " << user.students[index] << " " << user.day << " " << user.month << " " << acase << endl;
		}
		else
		{
			int j = 0;
			file.open("attendance.txt" , ios::out);

			garbage = "";
			for(i = 0 ; i < nWords ; j++ ,i++)
			{
				garbage = file_arr[i];
				file << garbage;
				if(j == 5)
				{
					file << endl;
					j=0;
				}
				else
					file << " ";
			}
		}
}

void attendance_get_date(int &month, int &day, int color)
{
	HANDLE  hConsole;
	hConsole = GetStdHandle (STD_OUTPUT_HANDLE);

	int day_cin, month_cin;
	bool fail = false;
	system("cls");
	cout << "Please enter the date to set attendance (type it digitally)" << endl;
	do
	{
		cout << "Day: ";
		SetConsoleTextAttribute (hConsole, color);
		cin >> day_cin;
		SetConsoleTextAttribute (hConsole, 15);
		fail = false;
		if (cin.fail() || day_cin < 0 || day_cin > 31) //handling cin failure
		{ 
			system("cls");
			fail = true;
			SetConsoleTextAttribute (hConsole, color);
			cout << "Your input is invalid \n";
			SetConsoleTextAttribute (hConsole, 15);
			cin.clear();
			cin.ignore(1000, '\n');
		}
	}while(fail == true);

	fail = false;

	day = day_cin;

	do
	{
		cout << "Month: ";
		SetConsoleTextAttribute (hConsole, color);
		cin >> month_cin;
		SetConsoleTextAttribute (hConsole, 15);
		fail = false;
		if (cin.fail() || month_cin < 0 || month_cin > 12) //handling cin failure(if the data is not digital)
		{ 
			system("cls");
			fail = true;
			SetConsoleTextAttribute (hConsole, color);
			cout << "Your input is invalid \n";
			SetConsoleTextAttribute (hConsole, 15);
			cin.clear();
			cin.ignore(1000, '\n');
		}
	}while(fail == true);

	month = month_cin;
}

void attendance(string mode,int color)
{
	Menu menu;
	menu.set_color(color);

	string garbage;
	int i = 0;
	bool fail = false, mawgood = false, next_page = false;
	int file_pointer = 0;

	HANDLE  hConsole;
	hConsole = GetStdHandle (STD_OUTPUT_HANDLE);

	garbage = todays_schedule(user.day, user.month);

	while(true)
	{
		fstream file("user.txt");

		if (!file)
		{
			// Print an error and exit
			cerr << "Uh oh, info.txt could not be opened!" << endl;
			exit(1);
		}
		for (int i = 0; i < 8; i++)
			user.students[i] = "";
		if (next_page == true)
			file.seekg(file_pointer, ios::beg);
		for(int i = 0; i < 8 || !EOF; i++)
		{
			file >> user.student >> user.type >> user.type;
			if(!file)
				break;// to avoid last file element redunduncy...
			if(user.type == "s" || user.type == "S")
				user.students[i] = user.student;
			else
				i--;
		}
		file_pointer = file.tellg();
		file.close();
		file.open("attendance.txt");

		if (!file)
		{
			// Print an error and exit
			cerr << "Uh oh, info.txt could not be opened!" << endl;
			exit(1);
		}

		for(int i=0; i<8; i++)
		{
			file.close();
			file.open("attendance.txt");
			while(file)
			{
				file >> attendance1.teacher>> attendance1.student >> attendance1.day >> attendance1.month >> attendance1.acase;

				if(attendance1.teacher == mode)
				{
					if(attendance1.student == user.students[i])
					{
						if(convertString(attendance1.day) == user.day && convertString(attendance1.month) == user.month)
						{
							user.students[i] += " ";
							user.students[i] += attendance1.acase;
							mawgood = true;
						}
					}
				}
			}
			if (mawgood == false && user.students[i] != "")
			{
				user.students[i] += " ";
				user.students[i] += "(Not set)";
			}
			mawgood = false;
		}

		if (user.students[6] != "")
		{
			switch(menu.Gmenu(user.students[0], user.students[1], user.students[2], user.students[3], user.students[4], user.students[5], user.students[6], "Next page", "Choose a specific date", "Home Page"))
			{
			case 0:
				{
					user.students[0] = "." + user.students[0];
					attendance_write(mode, color, user, attendance1); next_page = false; break;
				}
			case 1: 
				{
					user.students[1] = "." + user.students[1];
					attendance_write(mode, color, user, attendance1); next_page = false; break;
				}
			case 2: 
				{
					user.students[2] = "." + user.students[2];
					attendance_write(mode, color, user, attendance1); next_page = false; break;
				}
			case 3: 
				{
					user.students[3] = "." + user.students[3];
					attendance_write(mode, color, user, attendance1); next_page = false; break;
				}
			case 4: 
				{
					user.students[4] = "." + user.students[4];
					attendance_write(mode, color, user, attendance1); next_page = false; break;
				}
			case 5: 
				{
					user.students[5] = "." + user.students[5];
					attendance_write(mode, color, user, attendance1); next_page = false; break;
				}
			case 6: 
				{
					user.students[6] = "." + user.students[6];
					attendance_write(mode, color, user, attendance1); next_page = false; break;
				}
			case 7: next_page = true; break;
			case 8: attendance_get_date(user.month, user.day, color); next_page = false; break;//next page
			case 9: home_page(mode, color); break;  //home page
			}
		}
		else
		{
			switch(menu.Gmenu(user.students[0], user.students[1], user.students[2], user.students[3], user.students[4], user.students[5], user.students[6], user.students[7], "Choose a specific date" , "Home Page"))
			{
			case 0: 
				{
					user.students[0] = "." + user.students[0];
					attendance_write(mode, color, user, attendance1); next_page = false; break;
				}
			case 1: 
				{
					user.students[1] = "." + user.students[1];
					attendance_write(mode, color, user, attendance1); next_page = false; break;
				}
			case 2: 
				{
					user.students[2] = "." + user.students[2];
					attendance_write(mode, color, user, attendance1); next_page = false; break;
				}
			case 3: 
				{
					user.students[3] = "." + user.students[3];
					attendance_write(mode, color, user, attendance1); next_page = false; break;
				}
			case 4: 
				{
					user.students[4] = "." + user.students[4];
					attendance_write(mode, color, user, attendance1); next_page = false; break;
				}
			case 5: 
				{
					user.students[5] = "." + user.students[5];
					attendance_write(mode, color, user, attendance1); next_page = false; break;
				}
			case 6: 
				{
					user.students[6] = "." + user.students[6];
					attendance_write(mode, color, user, attendance1); next_page = false; break;
				}
			case 7: 
				{
					user.students[6] = "." + user.students[6];
					attendance_write(mode, color, user, attendance1); next_page = false; break;
				}

			case 8: attendance_get_date(user.month, user.day, color); next_page = false; break;
			case 9: home_page(mode, color); break;  //home page
			}
		}
		file.close();
	}
}

//**********************************************************************************
							//change theme.

void change_theme(int color)
{
	Menu menu;

	menu.set_color(color);

	switch (menu.Gmenu("red", "green", "turquoise", "yellow", "purple"))
	{
	case 0: menu.set_color(12); main_menu(12);
	case 1: menu.set_color(10); main_menu(10);
	case 2: menu.set_color(11); main_menu(11);
	case 3: menu.set_color(14); main_menu(14);
	case 4: menu.set_color(13); main_menu(13);
	}
}

//**********************************************************************************
							//authentication procedures.

string object(string user, string pass, string type, string mode)
{//determine wether the (username,password,subject) are available in "signup case" || wether the (username,password) ard valid in "signin case" or not.
	string user_check;
	string pass_check;
	string type_check;
	string subject_check;

	if (mode == "signup")
	{
		fstream file("user.txt");

		if (!file)
		{
			// Print an error and exit
			cerr << "Uh oh, info.txt could not be opened!" << endl;
			exit(1);
		}
		while (file >> user_check)//check if the username already exists.
		{
			file >> pass_check >> subject_check;
			if(user_check == user)
				return "eu";

			//check if another teacher teaches this subject.
			if(type == "math" || type == "physics" || type == "electronics" || type == "application" || type == "human" || type == "english" || type == "structural")
			{
				if(subject_check==type)
					return "es";
			}
		}
		file.close();

		file.open("user.txt", ios::app); //open file to save new data.

		file << user << " " << hashed(pass) << " " << type << endl;

		file.close();

		return "s";
	}
	else if(mode=="signin")
	{
		ifstream file("user.txt");

		if (!file)
		{
			// Print an error and exit
			cerr << "Uh oh, Info.txt could not be opened!" << endl;
			exit(1);
		}

		while (file >> user_check)//check input data validity...(username-password)
		{
			if(user_check == user)
			{
				file.seekg(1, ios::cur);
				file >> pass_check;
				if (pass_check == hashed(pass))
				{
					file.seekg(1, ios::cur);
					file >> type_check;
					file.close();
					if(type_check == "s" || type_check == "S")
						return user;
					else
						return type_check;
				}
				else 
				{
					system("cls");
					file.close();
					return "fp";
				}
			}
		}
		system("cls");
		return "fu";
	}
}

void signup(int color)
{
	char type;
	string username, subject, password, password2, condition, day, time;
	int c;

	Menu menu;

	menu.set_color(color);

	HANDLE  hConsole;
	hConsole = GetStdHandle (STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute (hConsole, 15);

	system("cls");

	cout << "Do not include spaces in the username" << endl;

	while(true)
	{
		cin.clear();

		cout << "username: ";
		SetConsoleTextAttribute (hConsole, color);
		cin >> username;
		cin.clear();
		if (username == "math" ||  username == "physics" || username == "electronics" || username == "application" || username == "human" || username == "english" || username == "structural")
		{
			system("cls");
			cout << "please enter a valid username";
		}
		else
			break;
	}

	SetConsoleTextAttribute (hConsole, 15);

	while (true)
	{
		cout << "Password: ";
		do   //Loop until 'Enter' is pressed
		{
			c = _getch();
			switch(c)
			{
			case 0:
				{
					_getch();
					break;
				}
			case '\b':
				{
					if(password.size() != 0)  //If the password string contains data, erase last character
					{
						cout << "\b \b";
						password.erase(password.size() - 1, 1);
					}
					break;       
				}   
			default:
				{
					if(isalnum(c) || ispunct(c)) //If the letter is digit letter or punctuation
					{
						password += c;
						SetConsoleTextAttribute (hConsole, color);
						cout << "*";
						SetConsoleTextAttribute (hConsole, 15);
					}
					break;     
				}      
			};
		}
		while(c != '\r');

		cout << endl << "password (again): ";
		do   //Loop until 'Enter' is pressed
		{
			c = _getch();
			switch(c)
			{
			case 0:
				{
					_getch();
					break;
				}
			case '\b':
				{
					if(password2.size() != 0)  //If the password string contains data, erase last character
					{
						cout << "\b \b";
						password2.erase(password2.size() - 1, 1);
					}
					break;       
				}   
			default:
				{
					if(isalnum(c) || ispunct(c)) //If the letter is digit letter or punctuation
					{
						password2 += c;
						SetConsoleTextAttribute (hConsole, color);
						cout << "*";
						SetConsoleTextAttribute (hConsole, 15);
					}
					break;     
				}      
			};
		}
		while(c != '\r');

		if (password != password2)
		{
			password = "";
			password2 = "";
			cout << endl << "you mistyped the password" << endl;
			menu.clear_screen(false);
			switch (menu.Gmenu("Sign in", "Sign up", "Change theme", "Exit"))
			{
			case 0: signin(color);
			case 1: signup(color);
			case 2: change_theme(color);
			case 3: exit(0);
			}
		}
		else break;
	}

	switch (menu.Gmenu("Teacher", "Student"))
	{
	case 0: type = 't'; break;
	case 1: type = 'S'; break;
	}

	if (type == 't' || type == 'T')
	{
		switch (menu.Gmenu("math", "physics", "electronics", "application software", "structural programming", "english", "human rights"))
		{
		case 0: condition = object(username, password, "math", "signup"); break;
		case 1: condition = object(username, password, "physics", "signup"); break;
		case 2: condition = object(username, password, "electronics", "signup"); break;
		case 3: condition = object(username, password, "application", "signup"); break;
		case 4: condition = object(username, password, "structural", "signup"); break;
		case 5: condition = object(username, password, "english", "signup"); break;
		case 6: condition = object(username, password, "human", "signup"); break;
		}
	}

	else
		condition = object(username, password, "S", "signup");

	if (condition == "eu")
	{
		system("cls");
		cout << "username already exists" << endl;
		menu.clear_screen(false);
		switch (menu.Gmenu("Sign in", "Sign up", "Change theme", "Exit"))
		{
		case 0: signin(color);
		case 1: signup(color);
		case 2: change_theme(color);
		case 3: exit(0);
		}
	}
	else if (condition == "es")
	{
		system("cls");
		cout << "Another teacher teaches this subject" << endl;
		menu.clear_screen(false);
		switch (menu.Gmenu("Sign in", "Sign up", "Change theme", "Exit"))
		{
		case 0: signin(color);
		case 1: signup(color);
		case 2: change_theme(color);
		case 3: exit(0);
		}
	}
	else
	{
		system("cls");
		cout<<"Now you can live a great educational experience :D ..alf alf mabrook assa7biee"<<endl;
		menu.clear_screen(false);
		switch (menu.Gmenu("Sign in", "Sign up", "Change theme", "Exit"))
		{
		case 0: signin(color);
		case 1: signup(color);
		case 2: change_theme(color);
		case 3: exit(0);
		}
	}
}

void signin(int color)
{
	string choice, username, password,check;
	string condition;
	int c;

	Menu menu;

	menu.set_color(color);

	menu.clear_screen(false);

	HANDLE  hConsole;
	hConsole = GetStdHandle (STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute (hConsole, 15);

	system("cls");

	cin.clear();
	cin.sync();

	cout << "username: ";
	SetConsoleTextAttribute (hConsole, color);
	cin >> username;
	SetConsoleTextAttribute (hConsole, 15);
	cout << "password: ";

	do   //Loop until 'Enter' is pressed
	{
		c = _getch();
		switch(c)
		{
		case '\b':
			{
				if(password.size() != 0)  //If the password string contains data, erase last character
				{
					cout << "\b \b";
					password.erase(password.size() - 1, 1);
				}
				break;       
			}   
		default:
			{
				if(isalnum(c) || ispunct(c)) //If the letter is digit letter or punctuation
				{
					password += c;
					SetConsoleTextAttribute (hConsole, color);
					cout << "*"; 
					SetConsoleTextAttribute (hConsole, 15);
				}
				break;     
			}      
		}
	}
	while(c != '\r');

	cout << endl;
	check = object(username, password, "s", "signin"); //check = "s" if type is (student) || "the subject" if type is (teacher).
	if(check == "fu") // wrong username.
	{
		cout << "Username is not correct" << endl;
		switch (menu.Gmenu("Sign in", "Sign up", "Change theme", "Exit"))
		{
		case 0: signin(color);
		case 1: signup(color);
		case 2: change_theme(color);
		case 3: exit(0);
		}
	}
	else if(check == "fp") // wrong password.
	{
		cout << "Password is not correct" << endl;
		switch (menu.Gmenu("Sign in", "Sign up", "Change theme", "Exit"))
		{
		case 0: signin(color);
		case 1: signup(color);
		case 2: change_theme(color);
		case 3: exit(0);
		}
	}
	else
	{
		system("cls");
		home_page(check, color);
	}
}

//***********************************************************************************
							//main.

void main_menu(int color)
{
	Menu menu;
	menu.set_color (color);

	switch (menu.Gmenu("Sign in", "Sign up", "Change theme", "Exit"))
	{
	case 0: signin(color);
	case 1: signup(color);
	case 2: change_theme(color);
	case 3: exit(0);
	}
}

void main() 
{
	welcome(12);
	main_menu(12);
}

//***********************************************************************************
